package ru.kolpashikov.firebaseandroid1.CommonClasses;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import ru.kolpashikov.firebaseandroid1.R;

public class MessageAdapter extends ArrayAdapter<Message> {
    public MessageAdapter(@NonNull Context context, int resource, @NonNull List<Message> objects) {
        super(context, resource, objects);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView == null){
            convertView = ((Activity)getContext()).getLayoutInflater().inflate(R.layout.item_message, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(R.id.messageTextView);
        Message message = getItem(position);
        textView.setText(message.getText());

        return convertView;
    }
}
