package ru.kolpashikov.firebaseandroid1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

import ru.kolpashikov.firebaseandroid1.CommonClasses.Message;
import ru.kolpashikov.firebaseandroid1.CommonClasses.MessageAdapter;

public class MainActivity extends AppCompatActivity {
    // 1. Пользовательский интерфейс
    private ListView mMessageListView;
    private EditText mMessageEditText;
    private Button mSendButton;
    private MessageAdapter mMessageAdapter;

    // 2. Firebase
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mMessageDatabaseReference;
    private ChildEventListener mChildEventListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 3. Связываем UI
        mMessageListView = (ListView)findViewById(R.id.listView);
        mMessageEditText = (EditText)findViewById(R.id.editText);
        mSendButton = (Button)findViewById(R.id.button);

        // 4. Инициализируем Firebase
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mMessageDatabaseReference = mFirebaseDatabase.getReference().child("messages");

        // 5. Создаем слушатель базы
        if(mChildEventListener == null){
            mChildEventListener = new ChildEventListener() {
                @Override
                public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                    Message message = dataSnapshot.getValue(Message.class);
                    mMessageAdapter.add(message);
                }

                @Override
                public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                }

                @Override
                public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            };
        }

        // 6. Устанавливаем слушатель базы
        mMessageDatabaseReference.addChildEventListener(mChildEventListener);

        // 7. Создаем лист, где будем хранить сообщения
        List<Message> messages = new ArrayList<>();

        // 8. Создаём и устанавливаем адаптер для сообщений
        mMessageAdapter = new MessageAdapter(this, R.layout.item_message, messages);
        mMessageListView.setAdapter(mMessageAdapter);

        // 9. Устанавливаем слушатель клика на кнопку, создаём сообщение, отправляем сообщение в базу, чистим ЕдитТекст
        mSendButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Message msg = new Message(mMessageEditText.getText().toString());
                mMessageDatabaseReference.push().setValue(msg);
                mMessageEditText.setText("");
            }
        });

        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.toString().trim().length() > 0){
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    //10. Удаляем слушатель базы данных
    @Override
    protected void onDestroy() {
        if(mChildEventListener != null){
            mMessageDatabaseReference.removeEventListener(mChildEventListener);
            mChildEventListener = null;
        }
        super.onDestroy();
    }
}
